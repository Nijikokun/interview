Titan Programmers Interview
=====
Hello potential programmers of Titan. 

I am Nijikokun, the leader of TPG. 
As with any group, job, project the members are required to have certain skills to produce quality code.

This Repository is meant to serve that purpose.

## The Task
Your task is to create a plugin for **Bukkit**, fork this repository,
fill the repository with the plugin source, and a built jar file.

## Criteria

### Plugin Name: 
 - **MossyBricks**
 
### Plugin Functionality
 - Show Plugin `name` and `version` in console onload.
 - Plugin should convert `cobblestone` to `mossy cobblestone` when clicked while user is holding `bone meal` or *optional item*
 - Plugin should also do this for `stone brick`
 - Upon converting the block you should consume the `bone meal` / *optional item* used.
 - The *optional item* should be changeable through an external `config.yml` or configuration file.

## Grading
You will be graded on the following aspects:

### Functionality
 - If the plugin does the job described above to the fullest extent.
 
### Java Knowledge
 - Making the plugin work and work correctly is one thing, doing it in a long method is not.
 
### Git Knowledge
 - Correctly forking and pushing files to fork without requesting a pull.
 
### Code Style
 - Keeping code sane and error free as well as visual acuity and seperation.
 - Keen on spaces versus tabs. (Yes, it matters) :P